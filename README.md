# myps.logger

Library to publish logs to ipc. Use with myps.broker. To view logs, use myps.logviewer.

```js
const Logger = require("myps.logger");

let myLogger = Logger({ source: "example", channel: "logs" });

myLogger.info("starting up");
myLogger.debug("trying to perform some operations");
myLogger.error("error while calculating route");
```
