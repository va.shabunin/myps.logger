const PubSubClient = require("myps.client");
const EE = require("events");

const Logger = params => {
  let self = new EE();

  if (!Object.prototype.hasOwnProperty.call(params, "source")) {
    throw new Error("Please define sourceName in params");
  }
  self.source = params.source;

  self.channel = "logs";
  if (Object.prototype.hasOwnProperty.call(params, "channel")) {
    self.channel = params.channel;
  }
  let _connected = false;
  let _queue = [];

  let _pubsubclient;
  if (Object.prototype.hasOwnProperty.call(params, "socketFile")) {
    _pubsubclient = PubSubClient({
      socketFile: params.socketFile
    });
  } else {
    _pubsubclient = PubSubClient();
  }
  _pubsubclient.on("connect", async connection_id => {
    self.connection_id = connection_id;
    self.emit("connect");
    _connected = true;
    _queue.forEach(async m => {
      await _pubsubclient.publish(self.channel, m);
    });
    _queue = [];
  });
  _pubsubclient.on("error", e => {
    self.emit("error", e);
  });

  _pubsubclient.on("end", _ => {
    self.emit("end");
  });

  _pubsubclient.on("close", _ => {
    self.emit("close");
  });

  let _log = async (level, payload) => {
    try {
      let messageObject = {
        level: level,
        source: self.source,
        timestamp: Date.now(),
        payload: payload
      };
      let messageString = JSON.stringify(messageObject);
      if (_connected) {
        await _pubsubclient.publish(self.channel, messageString);
      } else {
        // store in queue if not connected yet
        _queue.push(messageString);
      }
    } catch (e) {
      console.log(`Error publishing log: ${e.message}`);
    }
  };

  self.info = (...args) => {
    return _log("info", JSON.stringify(args));
  };
  self.debug = (...args) => {
    return _log("debug", JSON.stringify(args));
  };
  self.error = (...args) => {
    return _log("error", JSON.stringify(args));
  };

  self.reconnect = _ => {
    _pubsubclient.reconnect();
  };

  return self;
};

module.exports = Logger;
