const Logger = require("./index");

let myLogger = Logger({ source: "example", channel: "logs", socketFile: "/run/myps/myipc.sock" });

myLogger.info("starting up");
myLogger.debug("trying to perform some operations");
myLogger.error("error while calculating route");

setInterval(_ => {
  myLogger.info("hello, ", Math.floor(Math.random() * 1000));
}, 1000);

setInterval(_ => {
  myLogger.debug("friend");
}, 2500);
